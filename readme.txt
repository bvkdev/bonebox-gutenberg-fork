=== Bonebox Base ===
Contributors: BVK
Tags: one-column, full-width-template, custom-background, custom-header, custom-menu, editor-style, featured-images, flexible-header, microformats, rtl-language-support, sticky-post, threaded-comments, translation-ready, blog, custom-logo, block-styles
Requires at least: 5.0
Tested up to: 5.3

== Description ==

bonebox Base is a theme developed for creating custom, map-based guided tour content.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Copyright ==

bonebox WordPress Theme, Copyright (C) 2020, BVK

== Resources ==

* Based in part on Bones Development Theme; https://github.com/eddiemachado/bones/; License: WTFPL; License URI: http://sam.zoy.org/wtfpl/; 
* Based in part on Getwid Base Theme; https://getwid.getmotopress.com; License: GNU General Public License v2 or later; License URI: http://www.gnu.org/licenses/gpl-2.0.html
* Getwid based on Underscores https://underscores.me/, (C) 2012-2017 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html).
* Normalize.css https://necolas.github.io/normalize.css/, (C) 2012-2016 Nicolas Gallagher and Jonathan Neal, [MIT](https://opensource.org/licenses/MIT).
* Linearicons, (C) Linearicons, [CC BY-SA 4.0](https://linearicons.com/free#license).
